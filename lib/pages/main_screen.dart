import 'package:flutter/material.dart';
import 'package:timeclock/pages/shifts_screen.dart';
import '../dart/google_calendar_sync.dart';
import 'dart:async';

class MainScreen extends StatefulWidget {
  @override
  State createState() {
    return new MainScreenState();
  }
}

class MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Center(child: new Text("Timeclock App"),
        ),
      ),
      body: new ShiftsScreen(),
    );
  }

  @override
  void initState() {
    super.initState();
  }
}