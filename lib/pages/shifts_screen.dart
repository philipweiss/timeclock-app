import 'package:flutter/material.dart';
import '../dart/google_calendar_sync.dart';
import 'package:googleapis/calendar/v3.dart' as calendar;

class ShiftsScreen extends StatefulWidget {
  @override
  State createState() {
    return new ShiftsScreenState();
  }
}

class ShiftsScreenState extends State<ShiftsScreen> {
  GCal userCal = new GCal();
  List<calendar.CalendarListEntry> calendarsList;

  //itemBuilder for a shift list entry
  Widget shiftItemBuilder(BuildContext c, int index) {
    print(calendarsList.length);
    if (calendarsList != null && calendarsList[index].accessRole == "owner") {
      print("yep $index");
      return new Text(calendarsList[index].summary);
    }
    print("nope $index");
    return null;
  }

  void loadEvents() async {
    await userCal.handleSignIn();
    calendarsList = await userCal.getCalendarList();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Widget containerContent;
    if (calendarsList == null) {
      containerContent = new Text(
        "Loading, please wait ...",
        style: new TextStyle(fontSize: 20.0),
      );
    } else {
      containerContent = new ListView.builder(
          itemCount: calendarsList.length,
          itemBuilder: (BuildContext c, int index) {
            return shiftItemBuilder(c, index);
          });
    }

    return new Container(
      margin: EdgeInsets.all(10.0),
      child: containerContent,
    );
  }

  @override
  void initState() {
    super.initState();
    loadEvents();
  }
}
