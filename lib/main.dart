import 'package:flutter/material.dart';
import './pages/main_screen.dart';

void main() => runApp(new TimeclockApp(),
    );

class TimeclockApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Timeclock App",
      home: new MainScreen(),
    );
  }
}
