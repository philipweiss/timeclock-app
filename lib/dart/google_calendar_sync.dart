import 'package:googleapis/calendar/v3.dart' as calendar;
import 'package:google_sign_in/google_sign_in.dart';
import 'package:googleapis_auth/auth_io.dart' as auth;
import "package:http/http.dart" as http;

import "dart:async";

final scopes = [calendar.CalendarApi.CalendarScope];

class GCal {
  final GoogleSignIn _googleSignIn =
      new GoogleSignIn(scopes: [calendar.CalendarApi.CalendarScope]);
  GoogleSignInAccount _googleUser;
  GoogleSignInAuthentication _googleAuth;
  calendar.CalendarApi calendarApi;

  bool signedIn = false;

  Future<bool> handleSignIn() async {
    _googleUser = await _googleSignIn.signIn();

    _googleAuth = await _googleUser.authentication;

    auth.AccessToken token = auth.AccessToken("Bearer", _googleAuth.accessToken,
        DateTime.now().add(Duration(days: 1)).toUtc());
    auth.AccessCredentials(token, _googleUser.id, scopes);

    http.BaseClient _client = http.Client();
    auth.AuthClient _authClient = auth.authenticatedClient(
        _client, auth.AccessCredentials(token, _googleUser.id, scopes));
    calendarApi = new calendar.CalendarApi(_authClient);

    signedIn = true;
    print("signed in");
    return true;
  }

  Future<List<calendar.CalendarListEntry>> getCalendarList() async {
    if (!signedIn) {
      await handleSignIn();
    }

    calendar.CalendarList cals = await calendarApi.calendarList.list();
    return cals.items;
  }
}
